#!/usr/bin/env python
import sys

# Le format dans lequel on recoit les donnees est emploi\ttitre_emploi__nombre_postulant__diversite
# Nous allons donc construire une map qui a comme cle l'emploi et comme valeur une liste de tuple (titre_emploi, nombre_postulant, diversite)
result_map = {}



for line in sys.stdin:
    line = line.strip()
    emploi, titre_emploi_nombre_postulant_diversite = line.split("\t")
    (
        titre_emploi,
        nombre_postulant,
        diversite,
    ) = titre_emploi_nombre_postulant_diversite.split("__")
    if emploi in result_map:
        result_map[emploi].append((titre_emploi, nombre_postulant, diversite))
    else:
        result_map[emploi] = [(titre_emploi, nombre_postulant, diversite)]

# On peut calculer la moyenne de la diversite pour chaque emploi et afficher le resultat
for emploi in result_map:
    # On calcule la moyenne
    moyenne = 0
    somme_postulant = 0
    for titre_emploi, nombre_postulant, diversite in result_map[emploi]:
        moyenne += float(diversite)
        somme_postulant += int(nombre_postulant)
    if somme_postulant != 0: # on fait attention a ne pas diviser par 0
        moyenne /= somme_postulant
        # On affiche le resultat emploi-titre_emploi\tmoyenne
        # On afficher la moyenne avec 2 chiffres apres la virgule

        print("%s\t%s" % (emploi+"-"+titre_emploi, str(moyenne*100)[:4]+"%"))
